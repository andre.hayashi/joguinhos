
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  damage = 3,
  defense = 1,
}

