
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 16,
  damage = 3,
  defense = 2,
}

