local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'

local FightState = require 'common.class' (State)

function FightState:_init(stack)
    self:super(stack)
    self.action = nil
    self.encounter = nil
    self.character = nil
    self.target = nil
end

function FightState:enter(params)
    self.character = params.character
    self.encounter = params.encounter
    self.target = 1
    self:_show_cursor()
    self:_show_stats()
        self.character = params.character
        self.encounter = params.encounter
        self.target = 1
    if self.encounter[self.target]:get_hp() then
        self:_show_cursor()
        self:_show_stats()
    end
end

function FightState:_show_cursor()
    local atlas = self:view():get('atlas')
    local sprite_instance = atlas:get(self.encounter[self.target])
    local cursor = TurnCursor(sprite_instance)
    self:view():add('fight_cursor', cursor)
  end

function FightState:next_target()
    self.target = math.min(#self.encounter, self.target + 1)
    self:_show_cursor()
end

function FightState:previous_target()
    self.target = math.max(1, self.target - 1)
    self:_show_cursor()
end

function FightState:current_target()
    return self.target
end

function FightState:leave()
    local target_name = self.encounter[self.target]:get_name()
    local d = self.character:get_dmg()
    self.encounter[self.target]:change_hp(-d)
    if self.encounter[self.target]:get_hp() <= 0 then
        local message = ("%s killed %s"):format(self.character:get_name(), d, target_name)
        self:view():get('message'):set(message)
    else
        local message = ("%s deal %d damage to %s"):format(self.character:get_name(), d, target_name)
        self:view():get('message'):set(message)
    end
    self:view():remove('fight_cursor')
  end

function FightState:_show_stats()
    local bfbox = self:view():get('battlefield').bounds
    local position = Vec(bfbox.right + 16, bfbox.top)
    local char_stats = CharacterStats(position, self.character)
    self:view():add('char_stats', char_stats)
end

function FightState:on_keypressed(key)
    if key == 'return' then
        return self:pop()
    elseif key == 'down' then
        self:next_target()
    elseif key == 'up' then
        self:previous_target()
    end
  end

return FightState