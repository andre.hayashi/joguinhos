
local Character = require 'common.class' ()

function Character:_init(spec)
  self.spec = spec
  self.hp = spec.max_hp
  self.dmg = spec.damage
  self.def = spec.defense
end

function Character:get_name()
  return self.spec.name
end

function Character:get_appearance()
  return self.spec.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:get_dmg()
  return self.dmg, self.spec.damage
end

function Character:get_def()
  return self.def, spec.defense
end

function Character:change_hp(delta)
  self.hp = self.hp + delta
  self.hp = math.min(self.hp, self.spec.max_hp)
  self.hp = math.max(0, self.hp)
end

return Character

